# Node Hello World

Simple node.js app that servers "hello world"

Great for testing simple deployments to the cloud

## Configuration:

1. Add those variables to CICD Variables:

```
AWS_ACCESS_KEY_ID
AWS_DEFAULT_REGION
AWS_SECRET_ACCESS_KEY
BUCKET_NAME
```

2. Start a new pipeline