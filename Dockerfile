FROM node:15-alpine

WORKDIR  /hello
COPY . .
EXPOSE 3000
CMD npm start
